# Instructions to dockerize and deploy on a Minikube with _driver=none_-setting
Obviously, first clone the project. :-)

### **Start a local docker registry on the machine**

`docker run -d -p 5000:5000 --restart=always --name local-registry registry:2`

### **Build and tag the project**

`docker build -t localhost:5000/example-project .`

### **Push the image**

`docker push localhost:5000/example-project`

### **Test-Pull the image**

`docker pull localhost:5000/example-project`

### **Use it in your yaml**
```apiVersion: v1
kind: Pod
metadata:
  labels:
    app: example
  name: example-standalone-pod
spec:
  containers:
  - image: localhost:5000/example-project
    name: example-project-container
```


# Instructions on how to install and use HELM

Find information about HELM at https://helm.sh.

### **Download HELM**
`wget https://get.helm.sh/helm-v3.6.1-linux-amd64.tar.gz`

### **Extract it**
`tar -zxvf helm-v3.6.1-linux-amd64.tar.gz`

### **Copy it to the /bin-folder**
`mv linux-amd64/helm /usr/local/bin/helm`

### **Add a repository**
`repo add bitnami https://charts.bitnami.com/bitnami` 

### **Update the repository**
`helm repo update`

### **Install an application**
`helm install wordpress bitnami/wordpress`

### **Uninstall an application**
`helm uninstall wordpress`

### **Check the status of an application**
`helm status wordpress`

### **Create your own HELM chart**
`helm create my-chart`
