package io.cloudical.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnvApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnvApplication.class, args);
	}

}
